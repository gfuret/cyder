<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<title>Cynder</title>
	</head>
	<body>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
		<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/themes/smoothness/jquery-ui.css">
		<link rel="stylesheet" href="css/cynder.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
		<script src="js/mainpageloader.js"></script>

		<div class="container">
			<!-- Static navbar -->
	        <nav class="navbar navbar-default">
	        	<div class="container-fluid">
	          		<div class="navbar-header">
		            	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
		            	</button>
		            	<a href="#" class="navbar-brand" id="home">Cynder</a>
					</div>
					<div id="navbar" class="navbar-collapse collapse">
		            	<ul class="nav navbar-nav navbar-right">
		            		<li><a href="#" id="delete-list" >Delete matches</a></li>
		            		<li><a href="#" id="match-list" >Matches list</a></li>
		            		<li><a>Welcome, you</a></li>
		            	</ul>
	          		</div><!--/.nav-collapse -->
	        	</div><!--/.container-fluid -->
			</nav>
			<!--/.AJAX CONTENT -->
			<div id="mainloader"></div>
		</div>
		<script type="text/javascript">
			$(document).ready(
				function(){
					$( "#mainloader" ).load( "user_settings.php" );
					$( "#home" ).click(function(){ 
						location.reload(); 
					});
					$( "#delete-list" ).click(function(){ 
						deleteAllMatch();
					});	
					$( "#match-list" ).click(function(){ 
						$( "#mainloader" ).load( "match_list.php" );
					});					
				}					
			);
		</script>
	</body>
</html>