<?php

function saveMatchInCookie( $matchId )
{
	if( isset($_COOKIE["idmatches"]) ) 
	{
		//data: { id: idDragon, name: nameDragon, image: imageDragon, request: 'savematch' }
		$cookieValue = json_decode($_COOKIE["idmatches"]);
		if( in_array($matchId, $cookieValue) )
		{
			return $_COOKIE["idmatches"];
		}
		$cookieValue[] = $matchId;
		setcookie("idmatches", json_encode( $cookieValue ), time() + (86400 * 30), "/");		
	}
	else
	{
		setcookie("idmatches", json_encode( array($matchId) ), time() + (86400 * 30), "/");	
		return 'saved';
	}
	return $_COOKIE["idmatches"];
}


if(isset($_POST['request']))
{
	$requestType = $_POST['request'];

	switch ($requestType) 
	{
	    case 'savematch':
	    	//save user in match cookies
	    	if( isset($_POST['id']) )
	    	{
	    		$response = saveMatchInCookie( $_POST['id'] );
	    	}
	    	else
	    	{
	    		die;
	    	}
	        break;
	    case 'deletematch':
	    	//save user in match cookies
	        break;
	    case 'deleteallmatch':
	    	setcookie('idmatches');
	        break;
	    case 'getallmatch':
	    default:
	    	if(isset($_COOKIE["idmatches"]))
	    	{
	    		$response =  $_COOKIE["idmatches"] ;
	    	}
	    	else
	    	{
	    		$response =  json_encode(array());
	    	}
	        break;
	}
	echo $response;
}