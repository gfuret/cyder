function loadRandomDragons( gender )
{
	$("#mainloader").html('');
	$.ajax({
		method: "POST",
		url: "dragon_server_request.php",
		data: {gender: gender, request: 'random' }
	})
	.done(function( response ) 
	{
		friendDataObject = JSON.parse(response);
		htmlProfile(friendDataObject);
		$("#mainloader").append( $( '<div />',{"class":"bigger-centered-buttons"} ).append(
					
			$( '<button />', {	'data-gender':friendDataObject.gender, 
								'data-id':friendDataObject.id, 
								'data-name':friendDataObject.name, 
								'data-image':friendDataObject.image, 
								'data-likes':friendDataObject.likesYou, 
								'class':"bigger-round-button red-button burn",  
								'id':'dismiss',
								'html':'TO ASHES' } ),
			$( '<button />', {	'data-gender':friendDataObject.gender, 
								'data-id':friendDataObject.id, 
								'data-name':friendDataObject.name, 
								'data-image':friendDataObject.image, 
								'data-likes':friendDataObject.likesYou, 
								'class':"bigger-round-button blue-button match", 
								'id':'match',  
								'html':'LIGHT THE FLAME' } )
		));
	});
}

function deleteAllMatch()
{
	$.ajax({
		method: "POST",
		url: "match-manager.php",
		data: { request: 'deleteallmatch' }
	})
	.done(function( response ) 
	{
		//deleted
	});	
}

function saveMatch( idDragon, nameDragon, imageDragon )
{
	$.ajax({
		method: "POST",
		url: "match-manager.php",
		data: { id: idDragon, name: nameDragon, image: imageDragon, request: 'savematch' }
	})
	.done(function( response ) 
	{
		//$("<div> "+response+" </div>").dialog();
		console.log( response );
	});		
}

function getAllMatch()
{
	$.ajax({
		method: "POST",
		url: "match-manager.php",
		data: { request: 'getallmatch' }
	})
	.done(function( response ) 
	{		
		var matchedIdList = JSON.parse( response );
		$.each(matchedIdList, function(index, idDragon) 
		{
		
		    loadSpecificDragon( idDragon, 'matchedlist' );
		
		});
	});		
}

function loadSpecificDragon( idDragon, containerName )
{
	$.ajax({
		method: "POST",
		url: "dragon_server_request.php",
		data: {id: idDragon, request: 'specific' }
	})
	.done(function( response ) 
	{
		friendDataObject = JSON.parse(response);
		if(containerName == 'mainloader')
		{
			htmlProfile(friendDataObject);
		}
		if(containerName == 'matchedlist')
		{
			ulDragonList(friendDataObject);
		}
		
	});	
}

function ulDragonList(dragon)
{
	$("#matched-list").append(
		$('<div />',{'class':'listed', 'id': dragon.id }).append(
				$('<img  />',{'src':dragon.image, "alt":dragon.name, "class":"listed-image"}),
				$('<p  />',{"class":"listed-p"}).html( dragon.name )
		)
	);	
}

function htmlProfile(dragon)
{
	$("#mainloader").html('');
	$("#mainloader").append(
		$('<div />',{'class':'friend_profile', 'id': dragon.id }).append(
				$('<img  />',{'src':dragon.image, "alt":dragon.name, "class":"profile-pict"}),
				$('<h3  />', {'class':"stylish-message"}).html( dragon.name ),
				$('<p  />', {'class':"stylish-description"}).html( dragon.description )
		)
	);	
}


$(document).on('click',".listed",function()
{
	loadSpecificDragon( $(this).attr('id'), 'mainloader' );
});

$(document).on('click',"#dismiss",function()
{
	loadRandomDragons( $(this).data('gender') );
});

$(document).on('click',"#match",function()
{
	console.log( $(this).data('likes') 	);
	if( $(this).data('likes') == true )
	{
		saveMatch( $(this).data('id') );
		console.log('inside');
	}	
	loadRandomDragons( $(this).data('gender') );
});
